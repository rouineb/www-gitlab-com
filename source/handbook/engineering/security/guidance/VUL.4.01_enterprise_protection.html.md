---
layout: markdown_page
title: "VUL.4.01 - Enterprise Protection Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.4.01 - Enterprise Protection

## Control Statement

Where applicable and technically feasible, GitLab implements mechanisms to protect in-scope endpoints from threats such as malware and malicious actors.

## Context

This control outlines the components of a successfully deployed protection program which helps add another layer of risk mitigation to the GitLab environment. The applicability in this control is left vague since we have to apply some reason to this control. The intent of this control is to monitor and protect GitLab endpoints.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.


## Ownership

* gitlab.com and live production environments
  * Security Operations - responsible for maintenance and monitoring of Uptycs
  * Infrastructure - responsible for addressing Uptycs findings

* Laptops
  * Security Management- responsible for enforcement of endpoint management
  * IT-Ops - responsible for rolling out endpoint management tool, monitoring tool, reporting on non-compliance devices - trigger an alert

## Guidance

Any production systems we are not utilizing Uptycs or Fleetsmith on should have a documented justification for why it isn't applicable. It is fine to have different tools securing different systems, but the more different solutions we use, the more complexity we introduce into the maintenance of this control.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Enterprise Protection control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/942).

### Policy Reference

## Framework Mapping

* ISO
  * A.12.2.1
* SOC2 CC
  * CC6.8
  * CC7.1
* PCI
  * 5.1
  * 5.1.1
  * 5.1.2
  * 5.2
  * 6.2
