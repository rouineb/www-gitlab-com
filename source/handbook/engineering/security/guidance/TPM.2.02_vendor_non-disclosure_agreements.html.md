---
layout: markdown_page
title: "TPM.2.02 - Vendor Non-Disclosure Agreement Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TPM.2.02 - Vendor Non-Disclosure Agreement

## Control Statement

Requirements for confidentiality or non-disclosure agreements reflecting the organization’s needs for the protection of information shall be identified, regularly reviewed and documented.

## Context

This control provides GitLab a written, signed agreement with a vendor that non-disclosure of confidential information will be disseminated to outside parties.

## Scope

This control applies to all information shared with third parties that interact with the GitLab production environment.

## Ownership

Control Owner:

* Legal

Process Owner:

* Procurement

## Guidance

Maintain current copies of non-disclosure agreements between GitLab and vendors for each non-GitLab service used by the company where confidential information is shared with the vendor.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Vendor Non-Disclosure Agreement control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/927).

### Policy Reference

## Framework Mapping

* ISO
  * A.13.2.2
  * A.14.2.7
  * A.15.1.1
  * A.15.1.2
  * A.15.1.3
  * A.15.2.2
* PCI
  * 12.8.2
