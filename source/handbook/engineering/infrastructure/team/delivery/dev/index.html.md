---
layout: markdown_page
title: "Delivery Dev - Backend Engineer, Infrastructure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

* [Delivery team handbook](/handbook/engineering/infrastructure/team/delivery/)

## Overview

In addition to [Delivery team responsibilities](/handbook/engineering/infrastructure/team/delivery#top-level-esponsibilities), Delivery team also has members who are [Backend Engineer, Infrastructure](/job-families/engineering/backend-engineer/#infrastructure).

Main focus of said engineers is on reliability, performance and scalability, as well as resource optimization. These engineers work inside of the GitLab codebases with
a focus on optimization for GitLab.com that would benefit also all self-managed
users.

Backend Engineer, Infrastructure works from the [production request issue board](https://gitlab.com/groups/gitlab-org/-/boards/984386). The issue board shows highest impact
items that the engineers focus on as part of their daily tasks.

## Definition of Done

Backend Engineer, Infrastructure follows [the general definition of done](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/merge_request_workflow.md#definition-of-done) when working on various parts of
GitLab codebase.

Since the work carried out by engineers is from the perspective and focus on GitLab.com,
the definition of done is also expanded with:

1. The functionality is confirmed working on GitLab.com and has no negative impact on
reliability or performance of GitLab.com environments.
1. The functionality is documented in relevant additional documentation such as runbooks.
1. If relevant to them, support/abuse team(s) responsible for GitLab.com is informed of
existence of said functionality.
