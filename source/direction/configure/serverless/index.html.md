---
layout: markdown_page
title: "Category Vision - Serverless"
---

- TOC
{:toc}

## Serverless

Our vision is to make GitLab the preferred tool for developers and operators looking after an integrated continuous delivery and monitoring of serverless applications - no matter if those serverless applications are run inside a Kubernetes cluster or with a public serverless services provider.

## As a first step

Leveraging Knative and Kubernetes, users will be able to define and manage functions in GitLab. This includes the security, logging, scaling and costs of their serverless implementation for a particular project/group.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Serverless)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/155) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

### Automatic domains and TLS provisioning for serverless functions

We want to make it easy to get started with GitLab serverless. Having to purchase and configure a domain with TLS support makes it hard to write even simple, back-office scripts on top of serverless.

Please, contribute to the discussion in [our related epic](https://gitlab.com/groups/gitlab-org/-/epics/1920)

### Adding suppoprt for AWS Lambda

We want to serve well the largest serverless community, that is AWS users. Currently, we are investigating our opportunities and are looking for feedback from the community on Lambda usage.

Please, contribute to the discussion in [our related epic](https://gitlab.com/groups/gitlab-org/-/epics/1727)

## Ecosystem and Partners

### Google Kubernetes Engine

Google Kubernetes Engine (GKE) is a managed, production-ready environment for deploying containerized applications. It brings our latest innovations in developer productivity, resource efficiency, automated operations, and open source flexibility to accelerate your time to market. 

Users should be able to easily spin a new GKE cluster using GitLab to start using the GitLab serverless offering.

### AWS Lambda

AWS Lambda is a serverless compute service created by Amazon in 2015. It runs a function triggered by an event and manages the compute resources automatically so you don’t have to worry about what is happening under the hood.

### Azure Functions

Azure Functions is Microsoft’s response to Amazon’s Lambda. It offers a very similar product for the same cost. It uses Azure Web Jobs; the delay between hot cold invocations is less visible.

### Google Cloud Functions

It’s a fully managed nodeJS environment that will run your code handling scaling, security and performance. It’s event-driven and will trigger a function returning an event, very much in the same way AWS Lambda works. It’s intended to be used for small units of code that are placed under heavy load.

### Serverless Framework

The Serverless Framework is an open-source tool for managing and deploying serverless functions. It supports multiple programming languages and cloud providers. Its two main components are 1) [Event Gateway](https://serverless.com/event-gateway/), which provides an abstraction layer to easily design complex serverless applications, and 2)[Serverless Dashboard](https://serverless.com/dashboard/), for a better management of the application, as well as collaboration. Serverless Framework applications are written as YAML files (known as serverless.yml) which describe the functions, triggers, permissions, resources, and various plugins of the serverless application.

## Competition

### Netflix functions

### Dashbird

### Datadog

## Analyst landscape

The Serverless category is currently coupled with IaaS reports.

Gartner's `Magic Quadrant for Cloud Infrastructure as a Service` places AWS, Azure, and Google Cloud as leaders.

Forrester places `Serverless Computing` in their `Emerging Technology Spotlight` category, with the big three as leaders (AWS, Azure, Google Cloud)

## Top Customer Success/Sales issue(s)

n.a.

## Top user issue(s)

[Automatic domain for a serverless function](https://gitlab.com/gitlab-org/gitlab/issues/30151)
[SSL for Knative services](https://gitlab.com/gitlab-org/gitlab-ce/issues/56467)

## Top internal customer issue(s)

We collect [GitLab related issues under our dogfooding epic](https://gitlab.com/groups/gitlab-org/-/epics/1951).

## Top Vision Item(s) 

[Serveless Vision: primary user](https://gitlab.com/gitlab-org/gitlab/issues/32543)

