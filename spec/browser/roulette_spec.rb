require 'spec_helper'

describe 'the generated /roulette.json response' do
  before do
    visit '/roulette.json'
  end

  subject(:json_response) { JSON.parse(page.body) }

  it 'has the right content-type' do
    expect(page.response_headers['Content-Type']).to eq('application/json')
  end

  it 'includes name for all people' do
    is_expected.to all(include('name' => be_a(String)))
  end

  it 'includes username for all people' do
    is_expected.to all(include('username' => be_a(String)))
  end

  # Changing this spec means that the reviewer roulette part of GitLab's Danger
  # integration will also need to change
  it 'represents dmitriy correctly' do
    dmitriy = json_response[0]

    expect(dmitriy).to include(
      'username' => 'dzaporozhets',
      'name' => "Dmitriy 'DZ' Zaporozhets",
      'projects' => include(
        'gitlab' => 'maintainer backend'
      )
    )
  end

  # Changing this spec means that the reviewer roulette part of GitLab's Danger
  # integration will also need to change
  it 'represents multiple roles per project' do
    francisco = json_response.find { |r| r['username'] == 'fjsanpedro' }

    expect(francisco).to include(
      'projects' => include(
        'gitlab' => ['reviewer backend', 'trainee_maintainer database']
      )
    )
  end
end
